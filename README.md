# IDS 721: Weekly Miniproject 8: Command-line tool
- Author: Hiep Nguyen, hhn3
## Text Processing Tool

This is a simple Rust command-line tool that processes text by converting it to either uppercase or lowercase. It can take input directly as a string or read from a text file.

## Installing guideline

- Install Rust Cargo
- create a new project with bin
    ```bash
    cargo new rust-tool --bin
    ```
- In `main.rs` file:
    ```rust
    use std::env;
    use std::error::Error;
    use std::fs;
    use rust_tool::process_text;

    fn main() -> Result<(), Box<dyn Error>> {
        let args: Vec<String> = env::args().collect();

        if args.len() < 3 {
            eprintln!("Usage: {} <mode> <input>", args[0]);
            eprintln!("input: File path or string");
            eprintln!("mode: upper or lower");
            std::process::exit(1);
        }

        let input = &args[2];
        let mode = &args[1];

        let contents = if input.ends_with(".txt") {
            fs::read_to_string(input)?
        } else {
            input.clone()
        };

        let processed_contents = process_text(&contents, mode)?;

        println!("{}", processed_contents);

        Ok(())
    }
    ```
This main function take 3 arguments, the last twos are mode and input. The process function and test are written in `lib.rs` file:  
```rust    
pub fn process_text(input: &str, mode: &str) -> result<String, &'static str> {
    match mode {
        "upper" => Ok(input.to_uppercase()),
        "lower" => Ok(input.to_lowercase()),
        _ => Err("Invalid mode"),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_process_text_upper() {
        let result = process_text("hello", "upper").unwrap();
        assert_eq!(result, "HELLO");
    }

    #[test]
    fn test_process_text_lower() {
        let result = process_text("HELLO", "lower").unwrap();
        assert_eq!(result, "hello");
    }

    #[test]
    fn test_process_text_invalid_mode() {
        let result = process_text("hello", "invalid");
        assert!(result.is_err());
    }
}
```

## Usage

To use the tool, you need to provide two arguments: the input (a string or a path to a `.txt` file) and the mode (`upper` or `lower`).

### Direct String Input

- To convert a string to uppercase:
  ```bash
  cargo run -- upper "Hello, Rust!"
  ```

- To convert a string to lowercase
    ```bash
    cargo run -- "Hello, Rust!" lower
    ```

### File Input
- To convert the contents of a file (in `.txt`) to uppercase, first create a file (e.g., sample.txt) and then run:
    ```bash
    cargo run -- upper sample.txt
    ```
- To convert the contents of a file to lowercase:
    ```bash
    cargo run -- lower sample.txt
    ```

## Running Tests
This tool comes with a set of unit tests to ensure that the text processing logic works correctly.
The test can be found at /src/lib.rs along with the logic of the process method.
- To run the tests:
    ```bash
    cargo test
    ```

## Screenshot of test results:
![Test Results](image/Screenshot%202024-03-31%20at%204.31.10 PM.png)

## Screenshot of sample output:
![Test Results](image/Screenshot%202024-03-31%20at%204.44.07 PM.png)